import { 
  Provider
} from 'react-redux'

import store from 'redux-api/dist'

import TableData from './component/TableData'

const App = () => {
  return (
    <Provider
      store={store}
    >
      <TableData/>
    </Provider>
  )
}

export default App
