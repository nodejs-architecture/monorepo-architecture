import React from 'react'

import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Grid,
  Avatar
} from '@material-ui/core'

import { 
  useSelector
} from 'react-redux'

import ButtonAdd from 'module-a/dist/components/ButtonAdd'
import SideMenu from 'module-b/dist/components/SideMenu'

const TableData = () => {
  const users = useSelector((state) => state.users)

  return (
    <Grid
      container
    >
      <Grid
        item
        xs={12}
        style={{
          padding: '1em',
        }}
      >
        <ButtonAdd/>
      </Grid>
      <Grid
        item
        xs={12}
        style={{
          padding: '1em'
        }}
      >
        <Grid
          container
        >
          <Grid
            item
            md={8}
            xs={12}
          >
            <TableContainer>
              <Table>
                <TableHead>
                  <TableRow>
                    <TableCell/>
                    <TableCell>
                      <strong>
                        Nombre
                      </strong>
                    </TableCell>
                    <TableCell>
                      <strong>
                        Apellido
                      </strong>
                    </TableCell>
                    <TableCell>
                      <strong>
                        Teléfono
                      </strong>
                    </TableCell>
                    <TableCell>
                      <strong>
                        País
                      </strong>
                    </TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {users.map((row, index) => (
                    <TableRow
                      key={index}
                    >
                      <TableCell>
                        <Avatar
                          src={row.avatar}
                        />
                      </TableCell>
                      <TableCell>
                        {row.name}
                      </TableCell>
                      <TableCell>
                        {row.lastName}
                      </TableCell>
                      <TableCell>
                        {row.phone}
                      </TableCell>
                      <TableCell>
                        {row.country}
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </TableContainer>
          </Grid>
          <Grid
            item
            md={4}
            xs={12}
            style={{
              padding: '1em'
            }}
          >
            <SideMenu/>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  )
}

export default TableData
