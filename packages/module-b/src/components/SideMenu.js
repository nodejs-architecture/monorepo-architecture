import React from 'react'

import {
  List,
  ListItem,
  ListItemText,
  Divider
} from '@material-ui/core'

const menuItems1 = [
  {
    name: 'Menu 1'
  },
  {
    name: 'Menu 2'
  },
  {
    name: 'Menu 3'
  }
]

const menuItems2 = [
  {
    name: 'Menu 4'
  },
  {
    name: 'Menu 5'
  },
  {
    name: 'Menu 6'
  },
  {
    name: 'Menu 7'
  }
]

const SideMenu = () => (
  <List
    component="nav"
  >
    {menuItems1.map((item, index) => (
      <ListItem
        button
        key={index}
      >
        <ListItemText
          primary={item.name}
        />
      </ListItem>
    ))}
    <Divider/>
    {menuItems2.map((item, index) => (
      <ListItem
        button
        key={index}
      >
        <ListItemText
          primary={item.name}
        />
      </ListItem>
    ))}
  </List>)

export default SideMenu
