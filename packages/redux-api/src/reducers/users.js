import {
  ADD_USER
} from '../actionsTypes'

import faker from 'faker'

const initialState = [
  {
    avatar: faker.image.avatar(),
    name: 'Santiago',
    lastName: 'Sánchez',
    phone: '+593992637761',
    country: 'Ecuador'
  }
]

const users = (state = initialState, action) => {
  switch (action.type) {
    case ADD_USER:
      return [
        ...state,
        action.payload
      ]
    default:
      return state
  }
}

export default users
