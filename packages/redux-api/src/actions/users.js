import {
  ADD_USER
} from '../actionsTypes'

import faker from 'faker'

export const addUser = () => ({
  type: ADD_USER,
  payload: {
    avatar: faker.image.avatar(),
    name: faker.name.firstName(),
    lastName: faker.name.lastName(),
    phone: faker.phone.phoneNumber(),
    country: faker.address.country()
  }
})
