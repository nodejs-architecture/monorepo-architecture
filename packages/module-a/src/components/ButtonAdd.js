import React from 'react'

import {
  Button
} from '@material-ui/core'


import { 
  useDispatch
} from 'react-redux'

import {
  addUser
} from 'redux-api/dist/actions/users'

const ButtonAdd = () => {
  const dispatch = useDispatch()

  return (
    <Button
      color="primary"
      variant="contained"
      onClick={() => dispatch(addUser())}
    >  
      Add +
    </Button>
  )
}

export default ButtonAdd
