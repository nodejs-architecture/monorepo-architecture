import ButtonAdd from './components/ButtonAdd'

import {
  Provider 
} from 'react-redux'

import store from 'redux-api/dist'

const App = () => {
  return (
    <Provider
      store={store}
    >
      <ButtonAdd/>
    </Provider>
  )
}

export default App
